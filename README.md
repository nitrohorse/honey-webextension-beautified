# Honey WebExtension Beautified

This is the code for [Honey](https://www.joinhoney.com/)'s [webextension](https://addons.mozilla.org/en-US/firefox/addon/honey/) at v11.6.14. JavaScript has been beautified with [Beautifier](https://beautifier.io/). The open-source Firefox webextension [CRX Viewer](https://addons.mozilla.org/en-US/firefox/addon/crxviewer/) is also useful for viewing beautified extension code.

[Reference](https://old.reddit.com/r/privacytoolsIO/comments/f02sx9/i_want_the_honey_addon_how_do_i_ring_fence_it/).