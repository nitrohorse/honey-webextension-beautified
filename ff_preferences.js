const optionOn = 'h-toggle-main h-toggle-main50 h-toggle-on';
const optionOff = 'h-toggle-main h-toggle-main50 h-toggle-off';

const currSettings = JSON.parse(window.localStorage.getItem('device:settings')) || {};

function setSetting(setting, value) {
  currSettings[setting] = value;
  window.localStorage.setItem('device:settings', JSON.stringify(currSettings));
}

if (currSettings.dataShare === 'off') {
  document.getElementById('data-share').className = optionOff;
}
if (currSettings.allowCookies === 'off') {
  document.getElementById('allow-cookies').className = optionOff;
}
document.getElementById('data-share').addEventListener('click', () => {
  const currentlyOn = document.getElementById('data-share').className === optionOn;
  if (currentlyOn) {
    document.getElementById('data-share').className = optionOff;
    setSetting('dataShare', 'off');
  } else {
    document.getElementById('data-share').className = optionOn;
    setSetting('dataShare', 'on');
  }
});

document.getElementById('allow-cookies').addEventListener('click', () => {
  const currentlyOn = document.getElementById('allow-cookies').className === optionOn;
  if (currentlyOn) {
    document.getElementById('allow-cookies').className = optionOff;
    setSetting('allowCookies', 'off');
  } else {
    document.getElementById('allow-cookies').className = optionOn;
    setSetting('allowCookies', 'on');
  }
});
